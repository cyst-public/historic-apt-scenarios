from pagan.adversary import Quiver
from pagan.shared.primitives.tokens import Token, TokenType

from pagan.shared.primitives.types import DeviceHandle
from pagan.generation.scenario.scenario_automaton import NodeData, EdgeData, ScenarioStateMachine
from pagan.generation.preparation.accounts.account_generation import (
    BASIC_USER_CAPS_LOCAL,
    ADVANCED_USER_CAPS,
    RESTRICTED_ADMIN_CAPS,
    FULL_ADMIN_CAPS,
    SYSTEM_CAPS,
)

from pagan.shared.primitives.capabilities import (
    CapabilityTemplate,
    PrivilegeLevel,
    LimitType,
    CapabilityType,
)


def create_sandworm_graph() -> ScenarioStateMachine:
    sandworm_graph = ScenarioStateMachine()

    node1 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Attacker Machine"),
            attacker_capabilities={},
            attacker_tokens={
                Token(TokenType.ACCESS_VIA_NETWORK, device=DeviceHandle("Attacker Machine"))
            },
            available_credentials={},
            accepting_state=False,
        )
    )

    node2 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Caladan"),
            attacker_capabilities=node1.attacker_capabilities.union(
                {
                    CapabilityTemplate(
                        to=cap_type,
                        privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                        limit_type=LimitType.EXACT,
                        on_device=DeviceHandle("Caladan"),
                    )
                    for cap_type in BASIC_USER_CAPS_LOCAL
                    + ADVANCED_USER_CAPS
                    + RESTRICTED_ADMIN_CAPS
                    + FULL_ADMIN_CAPS
                }
            ),
            attacker_tokens=node1.attacker_tokens.union(
                {
                    Token(TokenType.SESSION, device=DeviceHandle("Caladan")),
                    Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("Caladan")),
                }
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    sandworm_graph.add_edge(
        node1,
        node2,
        EdgeData(
            Quiver.movement.exploit_open_application, service_enabler_pairs=("Weirdingway", "")
        ),
    )

    node3 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Caladan"),
            attacker_capabilities=node1.attacker_capabilities.union(
                {
                    CapabilityTemplate(
                        to=CapabilityType.PERSISTENCE,
                        privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                        limit_type=LimitType.EXACT,
                        on_device=DeviceHandle("Caladan"),
                    )
                }
            ),
            attacker_tokens=node1.attacker_tokens.union(
                {Token(TokenType.CAPABILITY, device=DeviceHandle("Caladan"))}
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    sandworm_graph.add_edge(
        node2, node3, EdgeData(Quiver.persistence.backdoor, service_enabler_pairs=("php", ""))
    )

    node4 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Caladan"),
            attacker_capabilities=node3.attacker_capabilities,
            attacker_tokens=node3.attacker_tokens.union(
                {Token(TokenType.ACCESS_VIA_NETWORK, device=DeviceHandle("Caladan"))}
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    sandworm_graph.add_edge(
        node3, node4, EdgeData(Quiver.discover_reachable, service_enabler_pairs=("bash", ""))
    )

    node5 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Caladan"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.LOCAL_SYSTEM,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("Caladan"),
                )
                for cap_type in SYSTEM_CAPS
            },
            attacker_tokens=node4.attacker_tokens.union(
                {Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("Caladan"))}
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    sandworm_graph.add_edge(
        node4,
        node5,
        EdgeData(
            Quiver.privilege_escalation.create_system_service, service_enabler_pairs=("systemd", "")
        ),
    )

    node6 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Caladan"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("Caladan"),
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
                + ADVANCED_USER_CAPS
                + RESTRICTED_ADMIN_CAPS
                + FULL_ADMIN_CAPS
            },
            attacker_tokens=node5.attacker_tokens.union(
                {Token(TokenType.CREDENTIAL, device=DeviceHandle("Caladan"))}
            ),
            available_credentials={Credential("admin", "Gammu")},
            accepting_state=False,
        )
    )

    sandworm_graph.add_edge(
        node5,
        node6,
        EdgeData(Quiver.credential_access.read_os_file, service_enabler_pairs=("bash", "")),
    )

    node7 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Gammu"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("Gammu"),
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
                + ADVANCED_USER_CAPS
                + RESTRICTED_ADMIN_CAPS
                + FULL_ADMIN_CAPS
            },
            attacker_tokens={
                Token(TokenType.SESSION, device=DeviceHandle("Gammu")),
                Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("Gammu")),
            },
            available_credentials=node6.available_credentials,
            accepting_state=False,
        )
    )
    sandworm_graph.add_edge(
        node6,
        node7,
        EdgeData(
            Quiver.movement.use_known_credentials, service_enabler_pairs={("ssh", ""), ("rdp", "")}
        ),
    )

    node8 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Gammu"),
            attacker_capabilities=node7.attacker_capabilities.union(
                {
                    CapabilityTemplate(
                        to=CapabilityType.PERSISTENCE,
                        privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                        limit_type=LimitType.EXACT,
                        on_device=DeviceHandle("Gammu"),
                    )
                }
            ),
            attacker_tokens=node7.attacker_tokens.union(
                {Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("Gammu"))}
            ),
            available_credentials=node7.available_credentials,
            accepting_state=False,
        )
    )

    sandworm_graph.add_edge(
        node7,
        node8,
        EdgeData(
            Quiver.privilege_escalation.elevated_autoexec,
            service_enabler_pairs=("TaskScheduler", ""),
        ),
    )

    node9 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Gammu"),
            attacker_capabilities=node8.attacker_capabilities,
            attacker_tokens=node8.attacker_tokens.union(
                {Token(TokenType.ACCESS_VIA_NETWORK, device=DeviceHandle("Gammu"))}
            ),
            available_credentials=node8.available_credentials,
            accepting_state=False,
        )
    )

    sandworm_graph.add_edge(
        node8, node9, EdgeData(Quiver.discover_reachable, service_enabler_pairs=("powershell", ""))
    )

    node10 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Gammu"),
            attacker_capabilities=node9.attacker_capabilities,
            attacker_tokens=node9.attacker_tokens.union(
                {Token(TokenType.CREDENTIAL, device=DeviceHandle("Gammu"))}
            ),
            available_credentials=node9.available_credentials.union(
                {Credential("domain", "Arrakis")}
            ),
            accepting_state=False,
        )
    )

    sandworm_graph.add_edge(
        node9,
        node10,
        EdgeData(Quiver.credential_access.input_capture, service_enabler_pairs=("", "")),
    )

    node11 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Gammu"),
            attacker_capabilities=node9.attacker_capabilities,
            attacker_tokens=node9.attacker_tokens.union(
                {Token(TokenType.CREDENTIAL, device=DeviceHandle("Gammu"))}
            ),
            available_credentials=node9.available_credentials.union(
                {Credential("domain", "Arrakis")}
            ),
            accepting_state=False,
        )
    )

    sandworm_graph.add_edge(
        node9,
        node11,
        EdgeData(Quiver.credential_access.dump_cached_creds, service_enabler_pairs=("lsass", "")),
    )

    node12 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Gammu"),
            attacker_capabilities=node9.attacker_capabilities,
            attacker_tokens=node9.attacker_tokens.union(
                {Token(TokenType.CREDENTIAL, device=DeviceHandle("Gammu"))}
            ),
            available_credentials=node9.available_credentials.union(
                {Credential("domain", "Arrakis")}
            ),
            accepting_state=False,
        )
    )

    sandworm_graph.add_edge(
        node9,
        node12,
        EdgeData(
            Quiver.credential_access.exploit_local_service, service_enabler_pairs=("chrome", "")
        ),
    )

    node13 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Arrakis"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("Arrakis"),
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
                + ADVANCED_USER_CAPS
                + RESTRICTED_ADMIN_CAPS
                + FULL_ADMIN_CAPS
            },
            attacker_tokens={
                Token(TokenType.SESSION, device=DeviceHandle("Arrakis")),
                Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("Arrakis")),
            },
            available_credentials=node12.available_credentials,
            accepting_state=False,
        )
    )

    sandworm_graph.add_edge(
        node10,
        node13,
        EdgeData(Quiver.movement.use_known_credentials, service_enabler_pairs=("rdp", "")),
    )
    sandworm_graph.add_edge(
        node11,
        node13,
        EdgeData(Quiver.movement.use_known_credentials, service_enabler_pairs=("rdp", "")),
    )
    sandworm_graph.add_edge(
        node12,
        node13,
        EdgeData(Quiver.movement.use_known_credentials, service_enabler_pairs=("rdp", "")),
    )

    node14 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Arrakis"),
            attacker_capabilities=node13.attacker_capabilities,
            attacker_tokens=node13.attacker_tokens.union(
                {Token(TokenType.ACCESS_VIA_NETWORK, device=DeviceHandle("Arrakis"))}
            ),
            available_credentials=node13.available_credentials,
            accepting_state=False,
        )
    )

    sandworm_graph.add_edge(
        node13,
        node14,
        EdgeData(Quiver.discover_reachable, service_enabler_pairs=("powershell", "")),
    )

    node15 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Arrakis"),
            attacker_capabilities=node14.attacker_capabilities,
            attacker_tokens=node14.attacker_tokens.union(
                {Token(TokenType.CREDENTIAL, device=DeviceHandle("Arrakis"))}
            ),
            available_credentials=node14.available_credentials.union({Credential("admin", "Host")}),
            accepting_state=False,
        )
    )

    sandworm_graph.add_edge(
        node14,
        node15,
        EdgeData(Quiver.credential_access.dump_cached_creds, service_enabler_pairs=("lsass", "")),
    )

    node16 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Arrakis"),
            attacker_capabilities=node14.attacker_capabilities,
            attacker_tokens=node14.attacker_tokens,
            available_credentials=node14.available_credentials,
            accepting_state=True,
        )
    )

    sandworm_graph.add_edge(
        node14, node16, EdgeData(Quiver.impact.manipulate_data, service_enabler_pairs=("", ""))
    )

    node17 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Host"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("Host"),
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
                + ADVANCED_USER_CAPS
                + RESTRICTED_ADMIN_CAPS
                + FULL_ADMIN_CAPS
            },
            attacker_tokens={
                Token(TokenType.SESSION, device=DeviceHandle("Host")),
                Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("Host")),
            },
        ),
        available_credentials=node15.available_credentials,
        accepting_state=False,
    )

    sandworm_graph.add_edge(
        node15,
        node17,
        EdgeData(Quiver.Movement.use_known_credentials, service_enabler_pairs=("rdp", "")),
    )

    node18 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Arrakis"),
            attacker_capabilities=node17.attacker_capabilities,
            attacker_tokens=node17.attacker_tokens.union(
                {Token(TokenType.ACCESS_VIA_NETWORK, device=DeviceHandle("Host"))}
            ),
            available_credentials=node17.available_credentials,
            accepting_state=False,
        )
    )

    sandworm_graph.add_edge(
        node17,
        node18,
        EdgeData(Quiver.discover_reachable, service_enabler_pairs=("powershell", "")),
    )

    node19 = sandworm_graph.add_node(
        NodeData(
            device=DeviceHandle("Arrakis"),
            attacker_capabilities=node18.attacker_capabilities,
            attacker_tokens=node18.attacker_tokens,
            available_credentials=node18.available_credentials,
            accepting_state=True,
        )
    )

    sandworm_graph.add_edge(
        node18, node19, EdgeData(Quiver.impact.manipulate_data, service_enabler_pairs=("", ""))
    )

    return sandworm_graph
