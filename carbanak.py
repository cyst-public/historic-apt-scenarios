from pagan.adversary import Quiver
from pagan.shared.primitives.tokens import Token, TokenType

from pagan.shared.primitives.types import DeviceHandle
from pagan.generation.scenario.scenario_automaton import NodeData, EdgeData, ScenarioStateMachine
from pagan.generation.preparation.accounts.account_generation import (
    BASIC_USER_CAPS_LOCAL,
    ADVANCED_USER_CAPS,
    RESTRICTED_ADMIN_CAPS,
    FULL_ADMIN_CAPS,
    SYSTEM_CAPS,
)

from pagan.shared.primitives.capabilities import (
    CapabilityTemplate,
    PrivilegeLevel,
    LimitType,
    CapabilityType,
)


def create_carbanak_graph() -> ScenarioStateMachine:
    carbanak_graph = ScenarioStateMachine()

    node1 = carbanak_graph.add_node(
        NodeData(
            device=DeviceHandle("Attacker Machine"),
            attacker_capabilities={},
            attacker_tokens={
                Token(TokenType.EXTERNAL_PHISHING_MAIL, device=DeviceHandle("Attacker Machine"))
            },
            available_credentials={},
            accepting_state=False,
        )
    )

    node2 = carbanak_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.USER,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("host A"),
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
            },
            attacker_tokens={
                Token(TokenType.SESSION, device=DeviceHandle("host A")),
                Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("host A")),
            },
            available_credentials={},
            accepting_state=False,
        )
    )

    carbanak_graph.add_edge(
        node1,
        node2,
        EdgeData(Quiver.movement.phishing_with_malware, service_enabler_pairs=("imap", "")),
    )

    node3 = carbanak_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.USER,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("host A"),
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
            },
            attacker_tokens={
                Token(TokenType.SESSION, device=DeviceHandle("host A")),
                Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("host A")),
            },
            available_credentials={},
            accepting_state=False,
        )
    )

    carbanak_graph.add_edge(
        node1,
        node3,
        EdgeData(
            Quiver.movement.pure_init_access.drive_by_compromise,
            service_enabler_pairs=("chrome", ""),
        ),
    )

    node4 = carbanak_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities=node3.attacker_capabilities.union(
                {
                    CapabilityTemplate(
                        to=CapabilityType.PERSISTENCE,
                        privilege_limit=PrivilegeLevel.USER,
                        limit_type=LimitType.EXACT,
                        on_device=DeviceHandle("host A"),
                    )
                }
            ),
            attacker_tokens=node3.attacker_tokens.union(
                {Token(TokenType.CAPABILITY, device=DeviceHandle("host A"))}
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    carbanak_graph.add_edge(
        node2, node4, EdgeData(Quiver.persistence.backdoor, service_enabler_pairs=("MS Office", ""))
    )
    carbanak_graph.add_edge(
        node3, node4, EdgeData(Quiver.persistence.backdoor, service_enabler_pairs=("", ""))
    )

    node5 = carbanak_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("host A"),
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
                + ADVANCED_USER_CAPS
                + RESTRICTED_ADMIN_CAPS
                + FULL_ADMIN_CAPS
            },
            attacker_tokens=node4.attacker_tokens.union(
                {Token(TokenType.CAPABILITY, device=DeviceHandle("host A"))}
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    carbanak_graph.add_edge(
        node4,
        node5,
        EdgeData(
            Quiver.privilege_escalation.exploit,
            service_enabler_pairs=("kernel-mode driver", "CVE-2013-3660"),
        ),
    )

    node6 = carbanak_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.LOCAL_SYSTEM,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("host A"),
                )
                for cap_type in SYSTEM_CAPS + {CapabilityType.PERSISTENCE}
            },
            attacker_tokens=node5.attacker_tokens.union(
                {Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("host A"))}
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    carbanak_graph.add_edge(
        node5,
        node6,
        EdgeData(
            Quiver.privilege_escalation.create_system_service, service_enabler_pairs=("systemd", "")
        ),
    )

    node7 = carbanak_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities=node6.attacker_capabilities,
            attacker_tokens=node6.attacker_tokens.union(
                {Token(TokenType.ACCESS_VIA_NETWORK, device=DeviceHandle("host A"))}
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    carbanak_graph.add_edge(
        node6, node7, EdgeData(Quiver.discover_reachable, service_enabler_pairs=("powershell", ""))
    )

    node8 = carbanak_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("host A"),
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
                + ADVANCED_USER_CAPS
                + RESTRICTED_ADMIN_CAPS
                + FULL_ADMIN_CAPS
            },
            attacker_tokens=node7.attacker_tokens.union(
                {Token(TokenType.CREDENTIAL, device=DeviceHandle("host A"))}
            ),
            available_credentials={Credential("admin", "host B")},
            accepting_state=False,
        )
    )

    carbanak_graph.add_edge(
        node7,
        node8,
        EdgeData(Quiver.credential_access.input_capture, service_enabler_pairs=("", "")),
    )

    node9 = carbanak_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("host B"),
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
                + ADVANCED_USER_CAPS
                + RESTRICTED_ADMIN_CAPS
                + FULL_ADMIN_CAPS
            },
            attacker_tokens={
                Token(TokenType.SESSION, device=DeviceHandle("host B")),
                Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("host B")),
            },
            available_credentials=node8.available_credentials,
            accepting_state=False,
        )
    )

    carbanak_graph.add_edge(
        node8,
        node9,
        EdgeData(Quiver.movement.use_known_credentials, service_enabler_pairs=("ssh", "")),
    )

    node10 = carbanak_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities=node9.attacker_capabilities,
            attacker_tokens=node9.attacker_tokens.union(
                {Token(TokenType.DATA, device=DeviceHandle("host B"))}
            ),
            available_credentials=node9.available_credentials,
            accepting_state=False,
        )
    )

    carbanak_graph.add_edge(
        node9, node10, EdgeData(Quiver.impact.collect_data, service_enabler_pairs=("", ""))
    )

    node11 = carbanak_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities=node10.attacker_capabilities,
            attacker_tokens=node10.attacker_tokens,
            available_credentials=node10.available_credentials,
            accepting_state=True,
        )
    )

    carbanak_graph.add_edge(
        node10, node11, EdgeData(Quiver.impact.manipulate_data, service_enabler_pairs=("", ""))
    )

    return carbanak_graph
