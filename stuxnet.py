from pagan.adversary import Quiver
from pagan.shared.primitives.tokens import Token, TokenType

from pagan.shared.primitives.types import DeviceHandle
from pagan.generation.scenario.scenario_automaton import NodeData, EdgeData, ScenarioStateMachine
from pagan.generation.preparation.accounts.account_generation import (
    BASIC_USER_CAPS_LOCAL,
    ADVANCED_USER_CAPS,
    RESTRICTED_ADMIN_CAPS,
    FULL_ADMIN_CAPS,
    SYSTEM_CAPS,
)

from pagan.shared.primitives.capabilities import (
    CapabilityTemplate,
    PrivilegeLevel,
    LimitType,
    CapabilityType,
)


def create_stuxnet_graph() -> ScenarioStateMachine:
    stuxnet_graph = ScenarioStateMachine()

    node1 = stuxnet_graph.add_node(
        NodeData(
            device=DeviceHandle("Attacker Machine"),
            attacker_capabilities={},
            attacker_tokens={
                Token(TokenType.PHYSICAL_ACCESS, device=DeviceHandle("Attacker Machine")),
                Token(
                    TokenType.ACCESS_VIA_REMOVABLE_MEDIA, device=DeviceHandle("Attacker Machine")
                ),
            },
            available_credentials={},
            accepting_state=False,
        )
    )

    node2 = stuxnet_graph.add_node(
        NodeData(
            device=DeviceHandle("Attacker Machine"),
            attacker_capabilities={},
            attacker_tokens=node1.attacker_tokens.union(
                {
                    Token(
                        TokenType.INFECTED_REMOVABLE_MEDIA, device=DeviceHandle("Attacker Machine")
                    ),
                }
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    stuxnet_graph.add_edge(
        node1,
        node2,
        EdgeData(
            Quiver.movement_preparation.infiltrate_infected_media, service_enabler_pairs=("", "")
        ),
    )

    node3 = stuxnet_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.USER,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("host A"),
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
            },
            attacker_tokens={
                Token(TokenType.SESSION, device=DeviceHandle("host A")),
                Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("host A")),
            },
            available_credentials={},
            accepting_state=False,
        )
    )

    stuxnet_graph.add_edge(
        node2,
        node3,
        EdgeData(
            Quiver.movement.use_infected_removable_media,
            service_enabler_pairs=("powershell", "CVE-2010-2568"),
        ),
    )

    node4 = stuxnet_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities=node3.attacker_capabilities,
            attacker_tokens=node3.attacker_tokes.union(
                {Token(TokenType.ACCESS_VIA_NETWORK, device=DeviceHandle("host A"))}
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    stuxnet_graph.add_edge(
        node3, node4, EdgeData(Quiver.discover_reachable, service_enabler_pairs=("powershell", ""))
    )

    node5 = stuxnet_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.SERVICE,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("host B"),
                )
                for cap_type in SERVICE_CAPS
            },
            attacker_tokens={
                Token(TokenType.SESSION, device=DeviceHandle("host B")),
                Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("host B")),
            },
            available_credentials={},
            accepting_state=False,
        )
    )

    stuxnet_graph.add_edge(
        node4,
        node5,
        EdgeData(
            Quiver.movement.exploit_open_application,
            service_enabler_pairs={("print spooler", "CVE-2010-2729"), ("smb", "CVE-2008-4250")},
        ),
    )

    node6 = stuxnet_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.LOCAL_SYSTEM,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("host B"),
                )
                for cap_type in SYSTEM_CAPS
            },
            attacker_tokens=node5.attacker_tokens.union(
                {Token(TokenType.CAPABILITY, device=DeviceHandle("host B"))}
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    stuxnet_graph.add_edge(
        node5,
        node6,
        EdgeData(
            Quiver.privilege_escalation.exploit,
            service_enabler_pairs={("TaskScheduler", "CVE-2010-3338")},
        ),
    )

    node7 = stuxnet_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities=node6.attacker_capabilities.union(
                {
                    CapabilityTemplate(
                        to=CapabilityType.PERSISTENCE,
                        privilege_limit=PrivilegeLevel.LOCAL_SYSTEM,
                        limit_type=LimitType.EXACT,
                        on_device=DeviceHandle("host B"),
                    )
                }
            ),
            attacker_tokens=node6.attacker_tokens.union(
                {Token(TokenType.CAPABILITY, device=DeviceHandle("host B"))}
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    stuxnet_graph.add_edge(
        node6,
        node7,
        EdgeData(Quiver.persistencePersistence.backdoor, service_enabler_pairs=("driver file", "")),
    )

    node8 = stuxnet_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities=node7.attacker_capabilities,
            attacker_tokens=node7.attacker_tokens.union(
                {Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("host B"))}
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    stuxnet_graph.add_edge(
        node7,
        node8,
        EdgeData(
            Quiver.privilege_escalation.component_hijacking,
            service_enabler_pairs=("Siemens Step 7", ""),
        ),
    )

    node9 = stuxnet_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities=node8.attacker_capabilities,
            attacker_tokens=node8.attacker_tokens,
            available_credentials={},
            accepting_state=True,
        )
    )

    stuxnet_graph.add_edge(
        node8,
        node9,
        EdgeData(Quiver.impact.manipulate_data, service_enabler_pairs=("powershell", "")),
    )

    return stuxnet_graph
