from pagan.adversary import Quiver
from pagan.shared.primitives.tokens import Token, TokenType

from pagan.shared.primitives.types import DeviceHandle
from pagan.generation.scenario.scenario_automaton import NodeData, EdgeData, ScenarioStateMachine
from pagan.generation.preparation.accounts.account_generation import (
    BASIC_USER_CAPS_LOCAL,
    ADVANCED_USER_CAPS,
    RESTRICTED_ADMIN_CAPS,
    FULL_ADMIN_CAPS,
    SYSTEM_CAPS,
)

from pagan.shared.primitives.capabilities import (
    CapabilityTemplate,
    PrivilegeLevel,
    LimitType,
    CapabilityType,
)


def create_nitro_graph() -> ScenarioStateMachine:
    nitro_graph = ScenarioStateMachine()

    node1 = nitro_graph.add_node(
        NodeData(
            device=DeviceHandle("Attacker Machine"),
            attacker_capabilities={},
            attacker_tokens={
                Token(TokenType.EXTERNAL_PHISHING_MAIL, device=DeviceHandle("Attacker Machine"))
            },
            available_credentials={},
            accepting_state=False,
        )
    )

    node2 = nitro_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("host A"),
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
                + ADVANCED_USER_CAPS
                + RESTRICTED_ADMIN_CAPS
                + FULL_ADMIN_CAPS
            },
            attacker_tokens={
                Token(TokenType.SESSION, device=DeviceHandle("host A")),
                Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("host A")),
            },
            available_credentials={},
            accepting_state=False,
        )
    )

    nitro_graph.add_edge(
        node1,
        node2,
        EdgeData(Quiver.movement.phishing_with_malware, service_enabler_pairs=("imap", "")),
    )

    node3 = nitro_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities=node2.attacker_capabilities.union(
                {
                    CapabilityTemplate(
                        to=CapabilityType.PERSISTENCE,
                        privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                        limit_type=LimitType.EXACT,
                        on_device=DeviceHandle("host A"),
                    )
                }
            ),
            attacker_tokens=node2.attacker_tokens.union(
                {Token(TokenType.CAPABILITY, device=DeviceHandle("host A"))}
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    nitro_graph.add_edge(
        node2, node3, EdgeData(Quiver.persistence.backdoor, service_enabler_pairs=("", ""))
    )

    node4 = nitro_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities=node3.attacker_capabilities,
            attacker_tokens=node3.attacker_tokens.union(
                {Token(TokenType.ACCESS_VIA_NETWORK, device=DeviceHandle("host A"))}
            ),
            available_credentials={},
            accepting_state=False,
        )
    )

    nitro_graph.add_edge(
        node3, node4, EdgeData(Quiver.discover_reachable, service_enabler_pairs=("powershell", ""))
    )

    node5 = nitro_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities=node4.attacker_capabilities,
            attacker_tokens=node4.attacker_tokens.union(
                {Token(TokenType.CREDENTIAL, device=DeviceHandle("host A"))}
            ),
            available_credentials={"admin", "host B"},
            accepting_state=False,
        )
    )

    nitro_graph.add_edge(
        node4,
        node5,
        EdgeData(Quiver.credential_access.dump_cached_creds, service_enabler_pairs=("lsass", "")),
    )

    node6 = nitro_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("host B"),
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
                + ADVANCED_USER_CAPS
                + RESTRICTED_ADMIN_CAPS
                + FULL_ADMIN_CAPS
            },
            attacker_tokens={
                Token(TokenType.SESSION, device=DeviceHandle("host B")),
                Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("host B")),
            },
            available_credentials=node5.available_credentials,
            accepting_state=False,
        )
    )

    nitro_graph.add_edge(
        node5,
        node6,
        EdgeData(Quiver.movement.use_known_credentials, service_enabler_pairs=("ssh", "")),
    )

    node7 = nitro_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities=node6.attacker_capabilities.union(
                {
                    CapabilityTemplate(
                        to=CapabilityType.PERSISTENCE,
                        privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                        limit_type=LimitType.EXACT,
                        on_device=DeviceHandle("host B"),
                    )
                }
            ),
            attacker_tokens=node6.attacker_tokens.union(
                {Token(TokenType.CAPABILITY, device=DeviceHandle("host B"))}
            ),
            available_credentials=node6.available_credentials,
            accepting_state=False,
        )
    )

    nitro_graph.add_edge(
        node6, node7, EdgeData(Quiver.persistence.backdoor, service_enabler_pairs=("rat", ""))
    )

    node8 = nitro_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities=node7.attacker_capabilities,
            attacker_tokens=node7.attacker_tokens.union(
                {Token(TokenType.DATA, device=DeviceHandle("host B"))}
            ),
            available_credentials=node7.available_credentials,
            accepting_state=False,
        )
    )

    nitro_graph.add_edge(
        node7, node8, EdgeData(Quiver.impact.collect_data, service_enabler_pairs=("powershell", ""))
    )

    node9 = nitro_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities=node8.attacker_capabilities,
            attacker_tokens=node8.attacker_tokens,
            available_credentials=node8.available_credentials,
            accepting_state=True,
        )
    )

    nitro_graph.add_edge(
        node8, node9, EdgeData(Quiver.impact.exfiltrate_data, service_enabler_pairs=("", ""))
    )

    return nitro_graph
