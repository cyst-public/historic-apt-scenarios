from pagan.adversary import Quiver
from pagan.shared.primitives.tokens import Token, TokenType

from pagan.shared.primitives.types import DeviceHandle
from pagan.generation.scenario.scenario_automaton import NodeData, EdgeData, ScenarioStateMachine
from pagan.generation.preparation.accounts.account_generation import (
    BASIC_USER_CAPS_LOCAL,
    ADVANCED_USER_CAPS,
    RESTRICTED_ADMIN_CAPS,
    FULL_ADMIN_CAPS,
    SYSTEM_CAPS,
)

from pagan.shared.primitives.capabilities import (
    CapabilityTemplate,
    PrivilegeLevel,
    LimitType,
    CapabilityType,
)


def create_darkseoul_graph() -> ScenarioStateMachine:
    darkseoul_graph = ScenarioStateMachine()

    node1 = darkseoul_graph.add_node(
        NodeData(
            device=DeviceHandle("Attacker Machine"),
            attacker_capabilities={},
            attacker_tokens={
                Token(TokenType.ACCESS_VIA_NETWORK, device=DeviceHandle("Attacker Machine"))
            },
            available_credentials={},
            accepting_state=False,
        )
    )

    node2 = darkseoul_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.LOCAL_SYSTEM,  # necessary for dumping the ntds file
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("host A"),
                )
                for cap_type in SYSTEM_CAPS
            },
            attacker_tokens={
                Token(TokenType.TokenType.SESSION, device=DeviceHandle("host A")),
                Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("host A")),
            },
            available_credentials={},
            accepting_state=False,
        )
    )

    darkseoul_graph.add_edge(
        node1,
        node2,
        EdgeData(
            Quiver.movement.exploit_open_application,
            service_enabler_pairs=("VMware Horizon", "CVE-2021-44228"),
        ),
    )

    node3 = darkseoul_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities=node2.attacker_capabilities,
            attacker_tokens=node2.attacker_tokens.union(
                {Token(TokenType.CREDENTIAL, device=DeviceHandle("host A"))}
            ),
            available_credentials={Credential("admin", "hostB")},
            accepting_state=False,
        )
    )

    darkseoul_graph.add_edge(
        node2,
        node3,
        EdgeData(Quiver.credential_access.dump_os_vault, service_enabler_pairs=("sam", "")),
    )

    node4 = darkseoul_graph.add_node(
        NodeData(
            device=DeviceHandle("host A"),
            attacker_capabilities=node3.attacker_capabilities,
            attacker_tokens=node3.attacker_tokens.union(
                {Token(TokenType.ACCESS_VIA_NETWORK, device=DeviceHandle("host A"))}
            ),
            available_credentials=node3.available_credentials,
            accepting_state=False,
        )
    )

    darkseoul_graph.add_edge(
        node3, node4, EdgeData(Quiver.discover_reachable, service_enabler_pairs=("powershell", ""))
    )

    node5 = darkseoul_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities={
                CapabilityTemplate(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                    limit_type=LimitType.EXACT,
                    on_device=DeviceHandle("host B"),
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
                + ADVANCED_USER_CAPS
                + RESTRICTED_ADMIN_CAPS
                + FULL_ADMIN_CAPS
            },
            attacker_tokens={
                Token(TokenType.SESSION, device=DeviceHandle("host B")),
                Token(TokenType.CAPABILITY_ALL, device=DeviceHandle("host B")),
            },
            available_credentials=node4.available_credentials,
            accepting_state=False,
        )
    )

    darkseoul_graph.add_edge(
        node4,
        node5,
        EdgeData(Quiver.movement.use_known_credentials, service_enabler_pairs=("ssh", "")),
    )

    node6 = darkseoul_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities=node5.attacker_capabilities.union(
                {
                    CapabilityTemplate(
                        to=CapabilityType.PERSISTENCE,
                        privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                        limit_type=LimitType.EXACT,
                        on_device=DeviceHandle("host B"),
                    )
                }
            ),
            attacker_tokens=node5.attacker_tokens.union(
                {Token(TokenType.CAPABILITY, device=DeviceHandle("host B"))}
            ),
            available_credentials=node5.available_credentials,
            accepting_state=False,
        )
    )

    darkseoul_graph.add_edge(
        node4,
        node5,
        EdgeData(Quiver.persistence.backdoor, service_enabler_pairs={("ssh", ""), ("rdp", "")}),
    )

    node7 = darkseoul_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities=node6.attacker_capabilities,
            attacker_tokens=node6.attacker_tokens,
            available_credentials=node6.available_credentials,
            accepting_state=True,
        )
    )

    darkseoul_graph.add_edge(
        node6, node7, EdgeData(Quiver.impact.manipulate_data, service_enabler_pairs=("", ""))
    )

    node8 = darkseoul_graph.add_node(
        NodeData(
            device=DeviceHandle("host B"),
            attacker_capabilities=node6.attacker_capabilities,
            attacker_tokens=node6.attacker_tokens.union(
                {Token(TokenType.IMPACT_DENIAL_OF_SERVICE, device=DeviceHandle("host B"))}
            ),
            available_credentials=node6.available_credentials,
            accepting_state=True,
        )
    )

    darkseoul_graph.add_edge(
        node7, node8, EdgeData(Quiver.impact.dos, service_enabler_pairs=("crypto miner", ""))
    )

    return darkseoul_graph
